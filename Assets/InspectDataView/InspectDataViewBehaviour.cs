using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yyyy.cccc.zzzz.framework
{
public class InspectDataViewBehaviour : MonoBehaviour
{
    public bool enableMaxWidth = true;
    public System.Action closeCallback;

    public int refFontSize = 20;
    public int refScreenWidth = 540;
    public Color bgColor = new Color(0, 0, 0, 0.8f);

    public InspectDataView inspectDataView = new InspectDataView();
    float oldTimeScale = 0;

    Texture2D tex;
    
    void OnEnable()
    {
        inspectDataView.Init();
        if(tex == null)
        {
            tex = new Texture2D(1, 1, TextureFormat.RGBA32, false, false);
        }
    }

    void OnDestroy()
    {
        if(oldTimeScale > 0)
        {
            Time.timeScale = oldTimeScale;
            oldTimeScale = 0;
        }
    }

    void OnGUI()
    {
        int fontSize2 = 0;

        var horizRatio = Screen.width / (float)refScreenWidth;
        var vertRatio = Screen.height / (float)refScreenWidth;
        var ratio = Mathf.Min(horizRatio, vertRatio);
        fontSize2 = (int)(refFontSize * ratio);

        

        int oldLabelFontSize = GUI.skin.label.fontSize;
        GUI.skin.label.fontSize = fontSize2;
        int oldButtonFontSize = GUI.skin.button.fontSize;
        GUI.skin.button.fontSize = fontSize2;
        int oldToggleFontSize = GUI.skin.toggle.fontSize;
        GUI.skin.toggle.fontSize = fontSize2;
        int oldTextFieldFontSize = GUI.skin.textField.fontSize;
        GUI.skin.textField.fontSize = fontSize2;

        float oldLabelFixHeight = GUI.skin.label.fixedHeight;
        GUI.skin.label.fixedHeight = fontSize2 + GUI.skin.label.padding.vertical + GUI.skin.label.margin.vertical;

        GUI.skin.toggle.fixedHeight = fontSize2 * 2;
        float  oldVerScrollBarWidth = GUI.skin.verticalScrollbar.fixedWidth;
        GUI.skin.verticalScrollbar.fixedWidth = fontSize2 * 2;

        float  oldVerScrollBarThumbWidth = GUI.skin.verticalScrollbarThumb.fixedWidth;
        GUI.skin.verticalScrollbarThumb.fixedWidth = fontSize2 * 2;

        float  oldHorScrollBarHeight = GUI.skin.horizontalScrollbar.fixedWidth;
        GUI.skin.horizontalScrollbar.fixedHeight = fontSize2 * 2;

        float  oldHorScrollBarThumbHeight = GUI.skin.horizontalScrollbarThumb.fixedWidth;
        GUI.skin.horizontalScrollbarThumb.fixedHeight = fontSize2 * 2;

        // var oldMatrix = GUI.matrix;
        // GUI.matrix = Matrix4x4.Scale(new Vector3(ratio, ratio, 1));

       

        try
        {
            float notchHeight = UnityEngine.Screen.height - UnityEngine.Screen.safeArea.yMax;
            

            tex.SetPixels(new Color[]{bgColor});
            tex.Apply();

            var oldBg = GUI.skin.box.normal.background;
            GUI.skin.box.normal.background = tex;
            GUILayout.Box("", GUILayout.Height(UnityEngine.Screen.height), GUILayout.Width(UnityEngine.Screen.width));
            GUI.skin.box.normal.background = oldBg;

            GUILayout.BeginArea(new Rect(0, notchHeight, UnityEngine.Screen.width, UnityEngine.Screen.height - notchHeight));


            // GUILayout.BeginVertical(GUILayout.MaxWidth(Screen.width / ratio));
            GUILayout.BeginVertical();

            if(Application.platform == RuntimePlatform.WebGLPlayer && notchHeight <= 0)
            {
                GUILayout.Label("");
                GUILayout.Label("");
            }

            GUILayout.BeginHorizontal();
            if(GUILayout.Button("close"))
            {
                closeCallback?.Invoke();
            }
            if(GUILayout.Button(oldTimeScale <= 0? "pause": "continue"))
            {
                if(oldTimeScale <= 0)
                {
                    oldTimeScale = Time.timeScale;
                    Time.timeScale = 0;
                }
                else
                {
                    Time.timeScale = oldTimeScale;
                    oldTimeScale = 0;
                }
            }
            GUILayout.EndHorizontal();

            inspectDataView.enableMaxWidth = enableMaxWidth;
            inspectDataView.ShowGUI();
            GUILayout.EndVertical();

            GUILayout.EndArea();
        }
        catch(System.Exception e)
        {
            throw e;
            // Debug.LogException(e);
        }
        finally
        {
            // GUI.matrix = oldMatrix;

            GUI.skin.label.fontSize = oldLabelFontSize;
            GUI.skin.label.fontSize = oldButtonFontSize;
            GUI.skin.textField.fontSize = oldTextFieldFontSize;
            GUI.skin.toggle.fontSize = oldToggleFontSize;

            GUI.skin.label.fixedHeight = oldLabelFixHeight;
            
            GUI.skin.verticalScrollbar.fixedWidth = oldVerScrollBarWidth;
            GUI.skin.verticalScrollbarThumb.fixedWidth = oldVerScrollBarThumbWidth;
            GUI.skin.horizontalScrollbar.fixedHeight = oldHorScrollBarHeight;
            GUI.skin.horizontalScrollbarThumb.fixedHeight = oldHorScrollBarThumbHeight;

            // if(Event.current.type != EventType.Repaint && Event.current.type != EventType.Layout)
            // {
            //     Event.current.Use();
            // }
        }
    }
}
}