using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace yyyy.cccc.zzzz.framework
{

public class CCCC
{
    public int ccbb;
    public string AAA;
    public SSSS sss;
}
public struct SSSS
{
    public int aaa;
    public string bbb;
    public CCCC ccc;

}

enum TestEnum
{
    A,
    B,
    C
}


[System.AttributeUsage(System.AttributeTargets.Class, Inherited = false)]
public class MyCustomAttribute : System.Attribute
{
    public MyCustomAttribute(string name)
    {
        Name = name;
    }

    public string Name { get; }
}

[System.AttributeUsage(System.AttributeTargets.Field, Inherited = false)]
public class MyCustom2Attribute : System.Attribute
{
    public MyCustom2Attribute(string name)
    {
        Name = name;
    }

    public string Name { get; }
}


[MyCustom("TestMyCustomAttribute")]
public class TestInspectDataView : MonoBehaviour
{
    public List<string> testList;
    public string[] testArray;
    public HashSet<string> testSet = new HashSet<string>(){"a", "b", "c"};
    public Dictionary<string, int> testMap = new Dictionary<string, int>(){{"a", 1}, {"b", 2}, {"c", 3}};


    public Rect testRect;
    public RectInt testRectInt;
    public RectOffset testRectOffest;

    int[][] testArr2d = new int[2][]{new int[]{1, 2, 3}, new int[]{4, 6}};
    int[,] testArr2d_2 = new int[2, 3]{{1, 2, 3}, {4, 5, 6}};

    [MyCustom2("TestMyCustom2Attribute")]
    const int testConst = 234;
    readonly int testReadOnly = 456;
    
    delegate int TestDelegate();
    TestDelegate testDelegate = () =>
    {
        return 1;
    };

    static int TestFunc()
    {
        return 2;
    }

    int TestFunc2()
    {
        return 2;
    }

    TestDelegate testDelegate2 = TestFunc;

    event TestDelegate testDelegate3 = TestFunc;

    TestEnum[] testEnums = new TestEnum[]{TestEnum.A, TestEnum.B};

    public class InnerClass
    {
        int a = 0;
    }
    


    public CCCC cc = new CCCC();
    // Start is called before the first frame update
    void Start()
    {
        testDelegate2 += TestFunc2;

        cc.ccbb = 4567;
        cc.AAA = "another string";
        cc.sss.aaa = 1234;
        cc.sss.bbb = "test string";
        cc.sss.ccc = cc;


        this.transform.SetParent(null);
        GameObject.DontDestroyOnLoad(this.gameObject);
    }
}

}