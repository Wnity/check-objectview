﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class Test
{
    [MenuItem("Custom/StartTest")]
    // Start is called before the first frame update
    public static void StartTest()
    {
        var a = typeof(Material).GetProperties(System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic);

        UnityEngine.Debug.LogError(a.Length);
    }

}
