# 这是一个用来在Player上查看c#所有数据的视图工具

<br/>

功能有：
- assembly模式：通过选择程序里的assembly的类型(type)查看类型(type)里的field和property等数据.
- scene模式：通过选择运行中的scene和GameObjects，查看field和property等数据.

<br/>

可查到的数据：
- 所有类型的field和property数据，CustomAttribute,类型的继续关系，interface,NestedType,以及System.Type里的数据等;
- 数组或泛型容器里的elements;
- delegate里的InvocationList;
- Unity的Scene, GameObject, Component;


<br/>

 # 示例视频:[https://www.bilibili.com/video/BV1xg4y1S7Dj/?vd_source=6cfd43a533c661fdd7e8df80d3b7ecc5](https://www.bilibili.com/video/BV1xg4y1S7Dj/?vd_source=6cfd43a533c661fdd7e8df80d3b7ecc5)