这是一个用来在Player上查看c#所有数据的视图工具，现在的功能有：
1.assembly模式：
	通过选择程序里的assembly的类型(type)查看类型(type)里的field和property等数据.
2.scene模式：
	通过选择运行中的scene和GameObjects，查看field和property等数据.


可查到的数据：
1.所有类型的field和property数据，CustomAttribute,类型的继续关系，interface,NestedType,以及System.Type里的数据等;
2.数组或泛型容器里的elements;
3.delegate里的InvocationList;
4.Unity的Scene, GameObject, Component;


后序还会增加资源相关的功能，敬请期待.